@extends('layout.app')
@section('content')
    <div class="container">
        <div class="text-center pt-1">
            <p class="h2 center">Kanban Board</p>
        </div>
      <div class="row mt-3 mb-3">
        <div class="col-sm-6">
          <h3>My <b>Tasks</b></h3>
        </div>
        <div class="col-sm-6 text-right">
          <button class="btn btn-outline-info add-task-form" data-toggle="modal" data-target="#addTaskForm">
            <i class="fa fa-plus" aria-hidden="true"></i> Add Task
          </button>
        </div>
      </div>
      <div class="row mb-3">
        @foreach($tasks as $status => $tasksByType)
          <div class="col-sm-4">
            <div class="card border-{{ App\Task::getTaskClass($status) }} h-100">
              <div class="card-header bg-{{ App\Task::getTaskClass($status) }} text-light text-uppercase">
                <b>{{ $status }}</b>
              </div>
              <div class="card-body pb-0 {{ $status }}">
                @foreach($tasksByType as $task)
                  @include('partials.tasks.card')
                @endforeach
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
    @include('partials.tasks.add')
    @include('partials.tasks.delete')
    @include('partials.tasks.edit')
    @include('partials.comments.add')
@endsection

