<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TasksController extends Controller {

  protected $rules = [
    'title' => 'required',
    'status' => 'required',
    'description' => 'required',
  ];

  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index() {
    $tasks = [
      'todo' => [],
      'processing' => [],
      'done' => [],
    ];

    foreach (Task::all()->sortByDesc('updated_at') as $task) {
      $tasks[$task->status][] = $task;
    }

    return view('tasks.index', compact('tasks'));
  }

  /**
   * Gets task data in json by api.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function getDataJson() {
    $tasks = [
      'todo' => [],
      'processing' => [],
      'done' => [],
    ];

    foreach (Task::all()->sortByDesc('updated_at') as $task) {
      $tasks[$task->status][] = $task;
    }

    return response()->json($tasks);
  }

  /**
   * Add task.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\JsonResponse
   * @throws \Throwable
   */
  public function store(Request $request) {
    $validator = Validator::make($request->input(), $this->rules);

    if ($validator->fails()) {
      return response()->json([
        'errors' => $validator->getMessageBag()->toArray(),
      ]);
    }

    $task = Task::add($request->all());
    $card = view('partials.tasks.card')->with('task', $task)->render();

    return response()->json([
      'task' => $task,
      'card' => $card,
    ]);
  }

  /**
   * Update task.
   *
   * @param  \Illuminate\Http\Request $request
   * @param $id
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request, $id) {
    $validator = Validator::make($request->input(), $this->rules);

    if ($validator->fails()) {
      return response()->json([
        'errors' => $validator->getMessageBag()->toArray(),
      ]);
    }

    $task = Task::find($id);
    $task->edit($request->all());

    $card = view('partials.tasks.card')->with('task', $task)->render();

    return response()->json([
      'task' => $task,
      'card' => $card,
    ]);

  }

  /**
   * Delete task.
   *
   * @param $id
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id) {
    $task = Task::findOrFail($id);
    $task->comments()->delete();
    $task->delete();

    return response()->json($task);
  }
}
